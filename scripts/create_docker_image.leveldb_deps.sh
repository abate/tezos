docker build -t tezos/leveldb -f scripts/Dockerfile.abuild.leveldb .

mkdir -p _docker_build_result/leveldb
mkdir -p _docker_build_result/keys

docker create --name tmp1 tezos/leveldb
docker cp -L tmp1:/etc/apk/keys _docker_build_result/
docker cp -L tmp1:/packages _docker_build_result/
docker rm tmp1
