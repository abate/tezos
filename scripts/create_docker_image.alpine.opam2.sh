#! /bin/sh

set -e

script_dir="$(cd "$(dirname "$0")" && echo "$(pwd -P)/")"
src_dir="$(dirname "$script_dir")"
cd "$src_dir"

. scripts/version.sh
image_name="${1:-tezos/opam2}"
image_version="${2:-alpine-${alpine_version}_ocaml-${ocaml_version}}"

cleanup () {
    set +e
    echo Cleaning up...
    rm -rf Dockerfile
}
trap cleanup EXIT INT

# assume tezos/leveldb has already been created
mkdir -p _docker_build_result/leveldb
mkdir -p _docker_build_result/keys
docker create --name tmp1 tezos/leveldb
docker cp -L tmp1:/etc/apk/keys _docker_build_result/
docker cp -L tmp1:/packages _docker_build_result/
docker rm tmp1

opam_tag=2.0.0-beta5

sed scripts/Dockerfile.alpine.opam2.in \
    -e 's|$alpine_version|'"$alpine_version"'|g' \
    -e 's|$ocaml_version|'"$ocaml_version"'|g' \
    -e 's|$opam_tag|'"$opam_tag"'|g' > Dockerfile

echo
echo "### Building base image..."
echo

docker build --pull -t "$image_name:$image_version" .

rm Dockerfile
