#!/bin/sh

# make sure to write files on the (eventually) exported
# volume with the same uid of the local user
USER_ID=${LOCAL_USER_ID:-9001}

USER_EXISTS=$(getent passwd "${USER_ID}")
if [ -z "${USER_EXISTS}" ]; then
  echo "Adding local UID : $USER_ID"
  adduser -D -u "${USER_ID}" tezos
fi

HOSTNAME=$(hostname)
: ${NODE:="/usr/local/bin/tezos-node"}
: ${CLIENT:="/usr/local/bin/tezos-client"}
: ${RUN_NODE:="true"}
: ${DEBUG:="false"}

# node specific
: ${PORT:="9732"}
: ${RPC_PORT:="8732"}
: ${DATADIR:="/home/tezos/data"}
: ${BOOTSTRAP_NODE:="false"}
: ${BOOTSTRAP_PEERS}
: ${ZERONET}

: ${SYSLOG_SERVER="syslog"}
: ${SYSLOG_PORT:="5514"}
: ${SYSLOG_PROTO:="tcp"}

if [ -n "${BAKER_PORT+set}" ] || [ -n "${BAKER_HOST+set}" ]; then
  RUN_BAKER="true"
fi
# baker / endorser
: ${BAKER_HOST:=${HOSTNAME}}
: ${BAKER_PORT:="8732"}

if [ -n "${ENDORSER_PORT+set}" ] || [ -n "${ENDORSER_HOST+set}" ]; then
  RUN_ENDORSER="true"
fi
: ${ENDORSER_HOST:=${HOSTNAME}}
: ${ENDORSER_PORT:="8732"}

export HOSTNAME NODE CLIENT
export PORT RPC_PORT DATADIR
export BOOTSTRAP_PEERS BOOTSTRAP_NODE ZERONET
export BAKER_HOST BAKER_PORT
export ENDORSER_HOST ENDORSER_PORT
export SYSLOG_SERVER SYSLOG_PORT SYSLOG_PROTO

su tezos -c "mkdir -p ${DATADIR}"

rm -f /etc/supervisord.d/*

if [ "${RUN_NODE}" == "true" ]; then
echo "Run node ${HOSTNAME}"
cat > /etc/supervisord.d/node.conf << EOF
[program:node]
user=tezos
command=/usr/bin/node.sh
priority=700
stdout_events_enabled = true
stderr_events_enabled = true
EOF
fi

cat > /etc/supervisord.d/init-client.conf << EOF
[program:init-client]
user=tezos
command=/usr/bin/client-identity.sh
priority=600
autostart=false
autorestart=false
startretries=0
redirect_stderr=false
redirect_stderr=false
EOF


if [ "${RUN_BAKER}" == "true" ]; then
echo "Run Baker ${HOSTNAME}"
cat > /etc/supervisord.d/baker.conf << EOF
[program:baker]
user=tezos
command=/usr/bin/baker.sh
priority=500
stdout_events_enabled = true
stderr_events_enabled = true
EOF
fi

if [ "${RUN_ENDORSER}" == "true" ]; then
echo "Run Endorser ${HOSTNAME}"
cat > /etc/supervisord.d/endorser.conf << EOF
[program:endorser]
user=tezos
command=/usr/bin/endorser.sh
priority=500
stdout_events_enabled = true
stderr_events_enabled = true
EOF
fi

supervisord --configuration /etc/supervisord.conf
