
# Docker tezos images

We build docker images to run tezos tests on the gitlab CI and to provide docker
images to end users.

## Base images

All images are based on the Alpine distribution. The basic image that is used as
base from all the other images is created using the script

    ./scripts/create_docker_image.alpine.opam1.sh

(this script is also run by the ``prepare`` stage of the gitlab CI)

This image uses opam version 1.x and correspond the the docker file

    scripts/Dockerfile.alpine.opam1.in

The same basic image using opam version 2.x is created by a different script

    ./scripts/create_docker_image.alpine.opam2.sh

based on the docker file scripts/Dockerfile.alpine.opam2.in

Both images contains the compiled opam{1,2} binaries and build dependencies

An intermediary image containing all the tezos build dependencis is created using the script

    ./scripts/create_docker_image.build_deps.sh

this image is based on the docker file scripts/Dockerfile.build_deps.in

This image contains the entire opam repository. To avoid rebuilding this image for each CI run,
the image is tagged using the SHA1 of all the dependencies. This image uses the alpine opam 1
base image.

Finally, the script ``scripts/create_docker_image.build.sh`` builds the tezos binaries.

## End User images

End User images are created by the script

    ./scripts/create_docker_image.minimal.sh

This image uses the docker file ``Dockerfile.minimal.in`` and pulls the tezos binaries from the
last tezos/tezos:build container.

We provide 2 type of images from the current branch:

- tezos/tezos:zeronet : This image is an un-initialized tesos images. It is a developent image
  and used only for debugging purposes. We provide scripts to create a local tezos network.

- tezos/tezos:alphanet : This image is initialized to join the alphanet. We apply
  a special patch to this image to join or bootstrap the testing tezos network. The network
  is rebooted frequently and all the transactions lost.

# Docker Images Scripts

Inside each image, we use supervisord to manage all running processes.

- scripts/docker/node.sh : initialize the node identity and run the node
  The node can be run in BOOTSTRAP mode, initializing the network, or as
  BOOTSTRAP_PEER mode and in this case, it will fetch the blockchain from
  a bootstrap.

- scripts/docker/endorser.sh : runs an endorser daemon
- scripts/docker/baker.sh : runs a baker daemon

## Bootstrapping zerozet/alphanet

Secret dictator keys, necessary to boostrap the {zero,alpha}-net are stored outside the
docker images. After running a node image, it is necessary to run a script to register the
dictator secret key and activate the alpha protocol.

The zeronet secret dictator key is stored in the file scripts/docker/zeronet-identities.inc.sh.
This key is used for local testing purposes.

Other keys are stored securely and only authorized sys admins have access to them.

The script scripts/docker/activate-net.sh is used to inject in a running docker contantainer the
secret credentials and initialize the network.

# Running zeroNet

The zeronet is used for local testing. We provide a docker-compose.yml file to inizialize multiple
nodes at once. Each node can run either a node, or/and a backer, or/and an endorser. Identities are
stored inside the image by default. Using a local volume it is possible to access the node and
client configuration and identities. The nodes run all process using the same uid of the current user.

    docker-compose up

## Initializing the zeronet

To bootstrap the zeronet you must start the node in sandbox mode. You can specify BOOTSTRAP_ZERONET
in the docker compose file. Once the node is running from the outside you need to manually add the
secret key to the node using the script

    sh scripts/docker/activate-net.sh 890573ac4f58

where 890573ac4f58 is the container id that is running

This script will use a special secret key that is paired with the pubkey distribuited in the node for
testing purposes.

## Initializing the alphanet

The alphanet is initialized in the same way using the a special secret key paired with the public key
hardcode in the protocol code. This operation is reserved to tezos admins.

# Running alphaNet

The alphanet is the tezos public testing network. The tezos admins take care of setting up 5 bootstrap
peers inizialized to work with a specific network version. This network is routinely reinitialized.
We provide Docker images to join the alpha network.

To run a docker container and join the alphanet run

    docker run tezos/tezos:alphanet

The node will create a new identity and start syncronzing with the tezos bootstrap peers. To have access
to the node/client identity the user must specify one enviroment variables as follows and create a local
directory to bind mount in the docker volume.

    mkdir -p $HOME/.tezos-alphanet/{client,node}
    export LOCAL_USER_ID=$(id -u)
    docker run tezos/tezos:alphanet -v "$HOME/.tezos-alphanet:/home/tezos/data"

# Running gitlab CI locally

We assume you have a working installation of docker. Then you need to install the
gitlab runner. This is the same daemon that runs on gitlab. It's already packaged
for the main Linux distributions.

    apt install gitlab-ci-multi-runner

To run the tests locally, the local user need to be part of the docker group, or
alternatively, the current command should be run using sudo.

It is possible to test the gitlab ci locally using the following command

    docker build -t tezos/docker -f scripts/Dockerfile.base .

    for i in "prepare:leveldb prepare:opam2 prepare:opam build publish:docker:minimal"; do \
      gitlab-ci-multi-runner exec shell $i \
        --env "CI_REGISTRY_IMAGE=test" \
        --env "CI_BUILDREF_NAME=zeronet" \
        --docker-pull-policy "if-not-present" \
        --docker-volumes /var/run/docker.sock:/var/run/docker.sock; \
    done

This will create all docker build and minimal images.

To run a specific test (in this case test:basic.sh) as defined in the file .gitlab-ci.yml

    gitlab-ci-multi-runner exec docker test:basic.sh \
      --env "CI_REGISTRY_IMAGE=test" \
      --docker-pull-policy "if-not-present" \
      --docker-volumes /var/run/docker.sock:/var/run/docker.sock
