#!/bin/sh

set -e
# all env variables passed by /usr/bin/entrypoint.sh

# wait for the node to boot
echo -n "Endorser: waiting for the node to come up "
while [ ! -f ${DATADIR}/node/bootstrap.done ]; do echo -n "."; sleep 2; done
echo "x"

sleep 5

${CLIENT} -base-dir ${DATADIR}/client -addr "${ENDORSER_HOST}" -port "${ENDORSER_PORT}" launch daemon -endorsement
