
# Runing tezos nodes using a docker-compose file

- LOCAL_USER_ID : this should be the local user. before running docker-composer
  run ``export LOCAL_USER_ID=$(id -u)``

## Bootstrap variables

- ZERONET: Run the node using a special sandbox indentities. This is used for testing.
- BOOTSTRAP_NODE: Run this node as a bootstrap node. If coupled with the variable 
  ``ZERONET=true`` the node will be consfigured to run a bootstrap node for a local
  tezos network.
- BOOTSTRAP_PEERS: a space separated list of bootstrap peers. this can be used together
  with ``ZERONET=true`` to bootstrap this node from a zeronet bootstrap node. If not 
  specified, the node will bootstrap using the tezos alphanet bootstrap peers.

## Node variables

- RUN_BAKER: Run the backer connecting to the local node
- RUN_ENDORSER: Run the endorser connecting to the local node
- BAKER_HOST / BAKER_PORT: Connect the backer to a different node
- ENDORSER_HOST / ENDORSER_PORT: Connect the endorser to a different node

## Syslog Variables

- SYSLOG_SERVER: the faqn address of the syslog host (default syslog)
- SYSLOG_PORT: the port port of the syslog host (default 5514)
- SYSLOG_PROTO: the syslog protocol (default tcp)

## Volumes

A node by default will initialize a new identity. To preserve and backup this identity
it is possible to bind mount the directory ``/home/tezos/data`` inside the docker image
to an externally accessile directory.

In order to do so, first you must create a local directory (e.g. ``~/.tezos``), then edit 
the docker compose file and adjust the ``volumes`` section of each node you intend to 
backup.

## Makefile

prepare the test environment and create the test images.

    make -f scripts/docker/Makefile prep

start the test images. Node and client identities are created if necessary

    make -f scripts/docker/Makefile start

activate the alphanet adding dictator keys and bootstrap accounts

    make -f scripts/docker/Makefile activate

Now the local alphanet is running. By default all logs are accessible in the 
directory ``tezos-nodes/syslog/`` . All nodes log to a single syslog instance.

The identities of clients and nodes are available in the directory ``tezos-nodes``

