#!/bin/sh

set -e
# all env variables passed by /usr/bin/entrypoint.sh

# wait for the node to boot
echo -n "Create Indentity: waiting for the node to come up "
while [ ! -f ${DATADIR}/node/bootstrap.done ]; do echo -n "."; sleep 2; done
echo "x"

# wait the node to boot up
sleep 5

echo "Initialize client identity for ${HOSTNAME}"

# generate a new identity if not present
if ! [ -e "${DATADIR}/client/config" ]; then
  mkdir -p "${DATADIR}/client"
  echo "Generating a new client identity ${DATADIR}/client"
  ${CLIENT} -addr "${HOSTNAME}" -port "${PORT}" \
    -base-dir "${DATADIR}/client" gen keys "${HOSTNAME}"
cat > ${DATADIR}/client/config << EOF
{
  "base_dir":"${DATADIR}/client",
  "node_addr":"${HOSTNAME}",
  "node_port":${PORT},
  "tls":false,
  "web_port":8080
}
EOF
else
  echo "Identity exists ${DATADIR}/client..."
fi

