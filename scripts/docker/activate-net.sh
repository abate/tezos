#! /usr/bin/env bash

set -e
set -x

CLIENT="/usr/local/bin/tezos-client -base-dir /home/tezos/data/client"
DOCKER_CONTAINER=$1

if [ -z "${DICTATOR_SECRET}" ]; then
  . scripts/docker/zeronet-identities.inc.sh
fi

if [ -n ${DOCKER_CONTAINER} ]; then

  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} add public key bootstrap1 ${BOOTSTRAP1_PUBLIC}"
  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} add secret key bootstrap1 ${BOOTSTRAP1_SECRET}"

  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} add public key bootstrap2 ${BOOTSTRAP2_PUBLIC}"
  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} add secret key bootstrap2 ${BOOTSTRAP2_SECRET}"

  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} add secret key dictator ${DICTATOR_SECRET}"

  docker exec ${DOCKER_CONTAINER} su tezos -c "${CLIENT} -block genesis activate protocol ProtoALphaALphaALphaALphaALphaALphaALphaALphaDdp3zK with fitness 1 and passes 1 and key dictator"
else
  echo "Usage $0: containerID"
fi
