#!/bin/sh

set -e

rm -f ${DATADIR}/node/bootstrap.done

# all env variables passed by /usr/bin/entrypoint.sh

DEBUGFLAG="-v"
if [ "${DEBUG}" = "true" ]; then
  DEBUGFLAG="-vv"
fi

rm -f "${DATADIR}/node/config.json"
ls -la ${DATADIR}
mkdir -p "${DATADIR}/node"

echo "Configure node"
"${NODE}" config --data-dir "${DATADIR}/node" init
"${NODE}" config \
  --data-dir "${DATADIR}/node" \
  --net-addr "${HOSTNAME}:${PORT}" \
  --rpc-addr "${HOSTNAME}:${RPC_PORT}" update

PEERS=""
if [ -n "${BOOTSTRAP_PEERS}" ]; then
  for peer in ${BOOTSTRAP_PEERS}; do
    PEERS="${PEERS} --peer=${peer}"
  done
fi

# generate a new identity if not present
if ! [ -e "${DATADIR}/node/identity.json" ]; then
  echo "Generating a new node identity for ${HOSTNAME}"
  "${NODE}" identity generate 24. \
    --data-dir "${DATADIR}/node"
else
  echo "Identity exists ${DATADIR}/node/identity.json..."
fi

touch ${DATADIR}/node/bootstrap.done

if [ "${ZERONET}" = "true" ]; then
  echo "Starting node (zeronet testing)"
  # For the zeronet we must specify the sandbox.json file with the genesis pub key
  if [ -n "${BOOTSTRAP_PEERS}" ]; then
    # if bootstrap peers are specified and in sandbox mode, we must specify the
    # peers also on the command line
    echo "Starting node zeronet with peers $peers"
    "${NODE}" run "${DEBUGFLAG}" \
      --no-bootstrap-peers \
      --data-dir "${DATADIR}/node" ${PEERS} \
      --sandbox=sandbox.json
  else
    # this run a sanboxed node without peers. By default it will not syncronize with
    # other peers. To force syncronization you must use a client rpc call as :
    # e.g. : ./tezos-client  rpc call /network/connect/172.18.0.3:9732
    echo "Starting node zeronet (bootstrap node)"
    "${NODE}" run "${DEBUGFLAG}" \
      --no-bootstrap-peers \
      --data-dir "${DATADIR}/node" \
      --sandbox=sandbox.json
  fi
else
  # we start the node normally. This will connect to their bootstrap peers and syncronize
  echo "Starting node alphanet"
  "${NODE}" run "${DEBUGFLAG}" \
    --data-dir "${DATADIR}/node" ${PEERS}
fi
