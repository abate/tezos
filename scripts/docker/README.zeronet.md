
# Running a local zeronet cluster.

To run a local zeronet cluster composed by a bootstrap node, two
nodes connecting as peers and a syslog instance, we prepared an
easy to use makefile. 

We assume that all docker images are available to the local docker
daemon. To initialize the zeronet, we first need to prepare its environment.

From the tezos source directory, we run the following command :

    make -f scripts/docker/Makefile prep

This will prepare the docker images tthought docker-composer and create the
test directories (by default in `tezos-nodes/`)

Next we start the test images.

    make -f scripts/docker/Makefile start

Node and client identities are created if necessary. All images are now running
in a private network initialized by the docker daemon.

To switch protocol, all nodes in the zeronet are started in sandbox mode, that is
we pass a special `sandbox.json` file containing the public key of the dictator and
the public key of 5 bootstrap accounts.

We use a script to activate the alphanet adding dictator and bootstrap account
secret keys to the client of the bootstrap node. And in the end, we activate the
alphanet protocol.

    make -f scripts/docker/Makefile activate

Now the local zeronet is running using the alphanet protocol. By default all logs 
are accessible in the directory ``tezos-nodes/syslog/`` . All nodes log to a single 
syslog instance.

The identities of clients and nodes are available in the directory ``tezos-nodes``

It is possible to get the ip address of each node using the following command. For
the bootstrap node :

    docker inspect \
      -f '{{range .NetworkSettings.Networks}}{{.IPAddress}}{{end}}' \
      $(docker ps -aqf "name=bootstrap")

from the docker host you can connect using the tezos client as usual 

    ./tezos-client -addr 172.18.0.5 -port 8732 get balance for bootstrap1

