#!/bin/sh

#set -e

# make sure to write files on the (eventually) exported
# volume with the same uid of the local user
USER_ID=${LOCAL_USER_ID:-9001}

USER_EXISTS=$(getent passwd "${USER_ID}")
if [ -z "${USER_EXISTS}" ]; then
  echo "Adding local UID : $USER_ID"
  adduser -D -u "${USER_ID}" syslog
fi

echo "Setting up"
chown -R syslog /usr/var/
mkdir -p /var/run/syslog-ng
mkdir -p /var/log/syslog-ng
chown -R syslog. /var/run/syslog-ng/
chown -R syslog. /var/log/syslog-ng/
chmod -R 0774 /var/log/syslog-ng
chmod -R 0774 /var/run/syslog-ng

echo "Run syslog-ng"
/usr/sbin/syslog-ng -u syslog -g syslog -F -f /etc/syslog-ng/syslog-ng.conf
