#!/bin/sh

set -e

# wait for the node to boot
echo -n "Baker: waiting for the node to come up "
while [ ! -f ${DATADIR}/node/bootstrap.done ]; do echo -n "."; sleep 2; done
echo "x"

sleep 5

${CLIENT} -base-dir "${DATADIR}/client" -addr "${BAKER_HOST}" -port "${BAKER_PORT}" launch daemon -baking -max-priority 64
